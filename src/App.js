import "./App.css";
import UserDetails from "./contexts/userContext";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import Dashboard from "./components/Dashboard";
import Login from "./components/Login";
import React, { Component, useContext } from "react";
import Settings from "./components/Settings";
import TodoDetails from "./components/TodoDetails";
import TodoInQuery from "./components/TodoInQuery";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <UserDetails>
          <Routes>
            <Route path="/login" element={<Login />} />
            <Route path="/" element={<Navigate to="/login" />} />
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/todo/:todoId" element={<TodoDetails />} />
            <Route path="/todo" element={<TodoInQuery />} />
            <Route path="/settings" element={<Settings />} />
          </Routes>
        </UserDetails>
      </BrowserRouter>
    );
  }
}

export default App;
