import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../contexts/userContext";
import validate from "../validate";

function Login(props) {
  const navigate = useNavigate();
  const [values, setValues] = useState({ userName: "", password: "" });
  const [error, setError] = useState({});
  const userDetails = useContext(UserContext);

  const handleChange = (value, key) => {
    const tempVal = { ...values, [key]: value };
    setValues(tempVal);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      values.userName === userDetails.userName &&
      values.password === userDetails.password
    ) {
      navigate("/dashboard");
    } else {
      const error = validate(values);
      if (Object.keys(error) === 0) {
      } else {
        setError(error);
      }
    }
  };

  return (
    <div className="form-container">
      <form className="form" onSubmit={handleSubmit}>
        <h1>LOGIN HERE..</h1>
        <div className="form-error">{Object.values(error)[0]}</div>

        <label>User Name</label>
        <input
          placeholder="Enter Name"
          type="name"
          value={values?.userName || ""}
          onChange={(event) => handleChange(event.target.value, "userName")}
        />
        <br />
        <label>Password</label>
        <input
          placeholder="Enter Password"
          type="password"
          value={values?.password || ""}
          onChange={(event) => handleChange(event.target.value, "password")}
        />
        <input className="btn-submit" type="submit"></input>
      </form>
    </div>
  );
}

export default Login;
