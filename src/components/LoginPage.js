import React, { useContext } from "react";
import Login, { LoginContext } from "./Login";

function LoginPage() {
  const { values, success, handleChange, handleSubmit } =
    useContext(LoginContext);

  return (
    <div className="form-container">
      <form className="form" onSubmit={handleSubmit}>
        <h1>LOGIN HERE..</h1>
        <label>User Name</label>
        <input
          placeholder="Enter Name"
          type="name"
          value={values?.UserName || ""}
          onChange={(event) => handleChange(event.target.value, "UserName")}
        />
        <br />
        <label>Password</label>
        <input
          placeholder="Enter Password"
          type="password"
          value={values?.Password || ""}
          onChange={(event) => handleChange(event.target.value, "Password")}
        />
        <input className="btn-submit" type="submit"></input>
      </form>
    </div>
  );
}

export default LoginPage;
