import React, { useContext, useState } from "react";
import { Link } from "react-router-dom";
import { UserContext } from "../contexts/userContext";

const Settings = () => {
  const userDetails = useContext(UserContext);

  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");

  const [oldUserName, setOldUserName] = useState("");
  const [oldPassword, setOldPassword] = useState("");
  const [prompt, setPrompt] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      oldUserName !== userDetails.userName ||
      oldPassword !== userDetails.password
    ) {
      setPrompt("Invalid UserName or Password");
    } else {
      userDetails.setUserDetails(userName, password);
      setPrompt("Password Changed! Go To Login.");
    }
  };

  return (
    <>
      <div className="settings-form">
        <form className="form-settings" onSubmit={handleSubmit}>
          <h2>Change Password</h2>
          <div className="settings-form-error">{prompt}</div>

          <div className="userName-div">
            <label>Old User Name</label>
            <input
              onChange={(e) => setOldUserName(e.target.value)}
              placeholder="Enter Old Password"
            />

            <label>New User Name</label>
            <input
              onChange={(e) => setUserName(e.target.value)}
              placeholder="Enter Username"
            />
          </div>

          <div className="password-div">
            <label>Old Password</label>
            <input
              onChange={(e) => setOldPassword(e.target.value)}
              placeholder="Enter Old Password"
            />

            <label>New Password</label>
            <input
              onChange={(e) => setPassword(e.target.value)}
              placeholder="Enter New Password"
            />
          </div>

          <div className="settings-button">
            <input type="submit" />
            <Link to="/login">
              <button>Go to login</button>
            </Link>
          </div>
        </form>
      </div>
    </>
  );
};

export default Settings;
