import React, { useState, useEffect } from "react";
// import { useParams } from "react-router-dom";
import { useSearchParams } from "react-router-dom";

function TodoInQuery() {
  const [todo, setTodo] = useState([]);
  const [error, setError] = useState("");

  let [searchParams, setSearchParams] = useSearchParams();

  useEffect(() => {
    let todoid = searchParams.get("id");
    fetch(`https://jsonplaceholder.typicode.com/todos/${todoid}`)
      .then((res) => {
        if (res.status === 404) {
          setError("404 --- 'TO-DO NOT FOUND'");
        }
        return res.json();
      })
      .then((res) => {
        setTodo(res);
      });
  }, [searchParams]);

  return (
    <div className="table-container">
      <div className="fetch-error">{error}</div>

      <table>
        <thead className="content-heading">
          <tr>
            <th>#id</th>
            <th>Task(s)</th>
            <th>Completed</th>
          </tr>
        </thead>
        <tbody>
          <React.Fragment key={todo.id}>
            <tr>
              <th className="id">{todo.id}</th>
              <th className="title">{todo.title}</th>
              <th>{todo.completed}</th>
            </tr>
          </React.Fragment>
        </tbody>
      </table>
    </div>
  );
}

export default TodoInQuery;
