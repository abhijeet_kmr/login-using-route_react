import React, { Component, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";

const Dashboard = (props) => {
  const [data, setData] = useState([]);
  const navigate = useNavigate();
  useEffect(() => {
    fetch(`https://jsonplaceholder.typicode.com/todos`)
      .then((res) => res.json())
      .then((res) => {
        setData(res.slice(0, 10));
      });
  }, []);
  const handleClick = (id) => {
    navigate(`/todo/${id}`);
  };

  return (
    <div className="table-container">
      <div className="settings-btn-div">
        <Link to="/settings">
          <button className="settings-btn">Settings</button>
        </Link>
      </div>
      <table>
        <thead className="content-heading">
          <tr>
            <th>#id</th>
            <th>Task(s)</th>
            <th>Completed</th>
          </tr>
        </thead>
        <tbody>
          {data.map((todo) => {
            return (
              <React.Fragment key={todo.id}>
                <tr
                  onClick={() => {
                    handleClick(todo.id);
                  }}
                  key={todo.id}
                >
                  <th className="id">{todo.id}</th>
                  <th className="title">{todo.title}</th>
                  <th>{todo.completed.toString()}</th>
                </tr>
              </React.Fragment>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default Dashboard;
