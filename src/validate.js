const validate = (values) => {
  const allFields = ["admin", "admin123"];
  let isError = {};
  allFields.forEach((item) => {
    if (item === "admin" && values.userName !== "admin") {
      isError[item] = "Incorrect User Name!";
    }
    if (item === "admin123" && values.password !== "admin123") {
      isError[item] = "Incorrect Password!";
    }
    console.log(item, values);
  });
  return isError;
};

export default validate;
