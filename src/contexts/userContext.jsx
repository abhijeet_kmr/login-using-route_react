import { createContext, Component } from "react";

export const UserContext = createContext();

class UserDetails extends Component {
  state = {
    userName: "admin",
    password: "admin123",
  };

  setUserDetails = (userName, password) => {
    this.setState({ userName: userName, password: password });
  };

  render() {
    return (
      <UserContext.Provider
        value={{ ...this.state, setUserDetails: this.setUserDetails }}
      >
        {this.props.children}
      </UserContext.Provider>
    );
  }
}

export default UserDetails;
